<?php

abstract class Account
{
    protected $username;

    function __construct($username)
    {
        $this->username = $username;
    }

    function login()
    {
        echo "\n" . $this->username . " logined";
    }

    function discountPercentage()
    {
        return 0.1;
    }
}

class User extends Account
{
    function __construct($username)
    {
        parent::__construct($username);
    }
}

class VipUser extends Account
{
    public function __construct($username)
    {
        parent::__construct($username);
    }

    function login()
    {
        parent::login();
        echo "\nnotify to email " . $this->username;
    }

    function discountPercentage()
    {
        return 0.3;
    }
}

function loginHandle(Account $acc)
{
    $acc->login();
}

function main()
{
    $normalUser = new User("user1");
    $vipUser = new VipUser("user2");
    loginHandle($normalUser);
    loginHandle($vipUser);
}

main();