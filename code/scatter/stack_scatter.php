<?php

class Stack
{

    protected $stack;
    protected $limit;

    public function __construct($limit = 10, $initial = array())
    {
        // initialize the stack
        $this->stack = $initial;
        // stack can only contain this many items
        $this->limit = $limit;
    }

    public function push($item)
    {
        // trap for stack overflow
        if (count($this->stack) < $this->limit) {
            // prepend item to the start of the array
            array_unshift($this->stack, $item);
        } else {
            throw new RunTimeException('Stack is full!');
        }
    }

    public function pop()
    {
        if ($this->isEmpty()) {
            // trap for stack underflow
            throw new RunTimeException('Stack is empty!');
        } else {
            // pop item from the start of the array
            return array_shift($this->stack);
        }
    }

    public function top()
    {
        return current($this->stack);
    }

    public function isEmpty()
    {
        return empty($this->stack);
    }

}

function main()
{
    $stack = new Stack();
    $stack->push(1);
    $stack->push(2);
    $stack->push(3);
    $stack->push(4);
    $stack->push(5);
    echo $stack->pop();
    //Auto complete here: $stack->
}