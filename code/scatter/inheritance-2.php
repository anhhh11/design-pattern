<?php

namespace inheritance_2;
// composition example
class DownloadService
{

}

class UploadService
{
    public function __construct(DownloadService $ds)
    {
    }
}

class ExportCsvService
{
    public function __construct(UploadService $us)
    {
    }
}

class AccountService
{

}

class OrderService
{
    public function __construct(ExportCsvService $ecs, AccountService $as)
    {
    }
}
