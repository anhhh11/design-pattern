function Stack(initial = [], limit = 10) {
  let stack = initial;

  function push(item) {
    if (stack.length < limit) {
      stack.push(item);
    } else {
      throw new Error("Stack is full!");
    }
  }

  function pop() {
    if (isEmpty()) {
      throw new Error("Stack is empty!");
    } else {
      return stack.pop();
    }
  }

  function top() {
    return stack[stack.length - 1];
  }

  function isEmpty() {
    return stack.length === 0;
  }

  return {push, pop, top, isEmpty};
}

function main() {
  let stack = Stack(); // No need to use `new` keyword here
  stack.push(1);
  stack.push(2);
  stack.push(3);
  stack.push(4);
  stack.push(5);
  console.log(stack.pop());
}

main();