<?php

interface Downloader
{
    function download(string $url): string;
}

class SingleDownloader implements Downloader
{

    function download(string $url): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}

function handleDownload(Downloader $downloader)
{
    echo $downloader->download('http://google.com.vn');
}

function main()
{
    $downloader = new SingleDownloader();
    handleDownload($downloader);
}

main();

class FakeDownloader implements Downloader
{
    public $is_called_download;

    function download(string $url): string
    {
        $this->is_called_download = true;
        return "";
    }
}

function test()
{
    $downloader = new FakeDownloader();
    handleDownload($downloader);
    assert($downloader->is_called_download, "Download not called");
}

test();
