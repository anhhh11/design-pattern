<?php

interface Download
{
    public function download(String $url): String;
}

class DownloadService implements Download
{
    private $url, $progress;

    public function __construct(String $url)
    {
        $this->url = $url;
    }

    public function download(String $url): String
    {
        // ...
        $this->progress += 1;
        // ...
        if ($this->progress === 100) {
            return "/temp/$url";
        }
    }
}

class FastDownloadService implements Download
{
    private $url, $progress, $parts, $totalNumberOfParts, $remainingParts;

    public function __construct(String $url)
    {
        $this->url = $url;
        $this->parts = [];
        $this->remainingParts = [];
        $this->totalNumberOfParts = 10;
    }

    public function download(String $url): String
    {
        // ...
        $this->progress += 1;
        // ...
        if ($this->progress === 100 && count($this->remainingParts) === 0) {
            return "/temp/$url";
        }
    }
}

class Client {
    public function __construct(Download $download){

    }
}
new Client(new FastDownloadService("http://example.com"));
