<?php

namespace Scatter;

class FormatAndPrintDocument
{
    public function printDocument($document)
    {
        print("Print document");
    }

    public function formatDocument($document)
    {
        print("Format document");
        return $document;
    }
}

class FormatDocument
{
    public function formatDocument($document)
    {
        print("Format document");
        return $document;
    }
}

class PrintDocument
{
    public function printDocument($document)
    {
        print("Print document");
    }
}


class SrpController
{
    public function main_NG()
    {
        $document = "some document";
        $formatPrintDocument = new FormatAndPrintDocument();
        $document = $formatPrintDocument->formatDocument($document);
        $formatPrintDocument->printDocument($document);
    }

    public function main_OK()
    {
        $formatter = new FormatDocument();
        $printer = new PrintDocument();
        $document = "some document";
        $document = $formatter->formatDocument($document);
        $printer->printDocument($document);
    }
}

(new SrpController())->main_NG();
(new SrpController())->main_OK();
