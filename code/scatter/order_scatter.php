<?php

namespace Scatter;

class Order
{

}

class OrderController_Original
{
    public function handlePlaceOrder()
    {
        // validate order
        // validate quantity
        // validate product
        // save order on db
        // send email to customer account
    }
}

class OrderController_RefactorExtractMethod
{
    public function handlePlaceOrder()
    {
        $order = new Order();
        $this->_validateOrder($order);
        $this->_placeOrder($order);
    }

    private function _validateOrder($order)
    {
        // validate quantity
        // validate product
    }

    private function _placeOrder($order)
    {
        // save order on db
        // send email to customer account
    }
}

class OrderController_RefactorExtractSideEffect
{
    public function handlePlaceOrder($order, $saveOrder, $sendOrderEmail)
    {
        $this->_validateOrder($order);
        $this->_placeOrder($order, $saveOrder, $sendOrderEmail);
    }

    private function _validateOrder($order)
    {
        // validate quantity
        // validate product
    }

    private function _placeOrder(Order $order, callable $saveOrder, callable $sendOrderEmail)
    {
        $saveOrder($order);
        $sendOrderEmail($order);
    }
}

class TestOrderController
{
    /***
     * @var OrderController_RefactorExtractSideEffect
     */
    private $orderController;

    public function setUp()
    {
        $this->orderController = new OrderController_RefactorExtractSideEffect();
    }

    public function TestPlaceOrder_OrderSavedToDbAndSentToEmail()
    {
        $validOrder = new Order();
        $orderSaved = false;
        $emailOrderSent = false;
        $saveOrder = function () use (&$orderSaved) {
            $orderSaved = true;
        };
        $sendOrderEmail = function () use (&$emailOrderSent) {
            $emailOrderSent = true;
        };
        $this->orderController->handlePlaceOrder($validOrder, $saveOrder, $sendOrderEmail);
        assert($orderSaved, "Order not saved to email");
        assert($emailOrderSent, "Order email not sent");
    }

    public function run()
    {
        $this->setUp();
        $this->TestPlaceOrder_OrderSavedToDbAndSentToEmail();
    }
}

(new TestOrderController())->run();
