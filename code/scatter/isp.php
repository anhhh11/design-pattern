<?php

namespace Scatter;

class LibPdfPrinter
{
    public function printDocument($document)
    {
        print("Print document: " . $document);
    }

    public function exportToFile($fileName)
    {
        print("Export to file " . $fileName);
    }
}

interface PdfPrinterInterface
{
    function printDocument($document);
}

class PdfPrinter implements PdfPrinterInterface
{
    private $printer;

    function __construct()
    {
        $this->printer = new LibPdfPrinter();
    }

    function printDocument($document)
    {
        $this->printer->printDocument($document);
    }
}

class FastLibPdfPrinter implements PdfPrinterInterface
{

    function printDocument($document)
    {
        print("Fast print document: " . $document);
    }
}


class IspController
{
    function main(PdfPrinterInterface $printer)
    {
        $printer->printDocument("Some document");
    }
}

(new IspController())->main(new PdfPrinter());
(new IspController())->main(new FastLibPdfPrinter());
