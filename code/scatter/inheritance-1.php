<?php

namespace inheritance_1;
// Inheritance example
class DownloadService
{

}

class UploadService extends DownloadService
{

}

class ExportCsvService extends UploadService
{

}

class AccountService
{

}

class OrderService extends ExportCsvService //, AccountService  // PHP not support multiple inheritance
{
}
