<?php

namespace Scatter;

interface PDFExporterInterface
{
    function export($document);
}

class PDFExporter implements PDFExporterInterface
{

    function export($document)
    {
        print("Export document " . $document);
    }
}

class ExportController_NG
{
    function main()
    {
        $pdfExporter = new PDFExporter();
        $pdfExporter->export("Some document");
    }
}

class ExportController
{
    function main(PDFExporterInterface $pdfExporter)
    {
        $pdfExporter->export("Some document");
    }
}

