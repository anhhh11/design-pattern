<?php

namespace OtherPattern\Builder;

class TextConverter
{
    private $elems = array();

    function convertCharacter($char)
    {
        array_push($this->elems, $char);
        return $this;
    }

    function convertFontCharacter($font)
    {
        array_push($this->elems, $font);
        return $this;
    }

    function convertParagraph($para)
    {
        array_push($this->elems, $para);
        return $this;
    }
}

class ASCIIText
{
    private $text;

    public function __construct($text)
    {
        $this->text = $text;
    }

    public function __toString()
    {
        return $this->text;
    }
}

class ASCIIConverter extends TextConverter
{
    private $elems = array();

    function convertToAscii($str)
    {
        // do some convert....
        return $str;
    }

    function convertCharacter($char)
    {
        array_push($this->elems, $this->convertToAscii($char));
    }

    function convertFontCharacter($font)
    {
        array_push($this->elems, $this->convertToAscii($font));
    }

    function convertParagraph($para)
    {
        array_push($this->elems, $this->convertToAscii($para));
    }

    function getAsciiText(): ASCIIText
    {
        return new ASCIIText(implode(" ", $this->elems));
    }
}

class HTMLText
{
    private $text;

    public function __construct($text)
    {
        $this->text = $text;
    }

    public function __toString()
    {
        return '<html>' . $this->text . '</html>';
    }
}

class HTMLConverter extends TextConverter
{
    private $elems = array();

    function wrapP($str)
    {
        return '<p>' . $str . '</p>';
    }

    function convertCharacter($char)
    {
        array_push($this->elems, $this->wrapP($char));
    }

    function convertFontCharacter($font)
    {
        array_push($this->elems, $this->wrapP($font));
    }

    function convertParagraph($para)
    {
        array_push($this->elems, $this->wrapP($para));
    }

    function getHTMLText(): HTMLText
    {
        return new HTMLText(implode(" ", $this->elems));
    }
}

class TextWidget
{
    private $text;

    public function __construct($text)
    {
        $this->text = $text;
    }

    public function __toString()
    {
        return $this->text;
    }
}

class TextWidgetConverter extends TextConverter
{
    private $elems = array();

    function wrapText($str)
    {
        return '---' . $str . '---';
    }

    function convertCharacter($char)
    {
        array_push($this->elems, $this->wrapText($char));
    }

    function convertFontCharacter($font)
    {
        array_push($this->elems, $this->wrapText($font));
    }

    function convertParagraph($para)
    {
        array_push($this->elems, $this->wrapText($para));
    }

    function getTextWidget(): TextWidget
    {
        return new TextWidget(implode(" ", $this->elems));
    }
}

class DocReader
{
    private $builder;

    public function __construct(ASCIIConverter $builder)
    {
        $this->builder = $builder;
    }

    public function parse()
    {
        $this->builder->convertCharacter("123");
        $this->builder->convertFontCharacter("123");
        $this->builder->convertParagraph("Hello");
        echo $this->builder->getAsciiText();
    }
}
