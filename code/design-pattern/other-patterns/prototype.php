<?php

namespace OtherPattern\Prototype;
abstract class Tool
{
    abstract function manipulate();
}

abstract class Graphic
{
    function draw($position)
    {
    }

    abstract function clone(): Graphic;
}

class RotateTool extends Tool
{
    private $graphic;

    public function __construct(Graphic $graphic)
    {
        $this->graphic = $graphic;
    }

    function manipulate()
    {
        $cloned = $this->graphic->clone();
        // do something with cloned graphic
        return $cloned;
    }
}

class GraphicTool extends Tool
{
    private $graphic;

    public function __construct(Graphic $graphic)
    {
        $this->graphic = $graphic;
    }

    function manipulate()
    {
        $cloned = $this->graphic->clone();
        // do something with cloned graphic
        return $cloned;
    }
}


class ChairGraphic extends Graphic
{

    function clone(): Graphic
    {
        return new ChairGraphic();
    }
}

class TableGraphic extends Graphic
{

    function clone(): Graphic
    {
        return new TableGraphic();
    }
}

class CustomGraphic extends Graphic
{

    function clone(): Graphic
    {
        return new CustomGraphic();
    }
}
