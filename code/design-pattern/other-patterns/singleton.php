<?php

namespace OtherPattern\Singleton;
class Singleton
{
    private static $uniqueInstance;
    private $singletonData;

    public static function instance(): Singleton
    {
        if (!isset(Singleton::$uniqueInstance)) {
            Singleton::$uniqueInstance = new Singleton();
        }
        return Singleton::$uniqueInstance;
    }

    public function singletonOperation()
    {
        echo "Do something";
    }

    public function getSingletonData()
    {
        return $this->singletonData;
    }
}

function main()
{
    $singleton = Singleton::instance();
    $singleton->singletonOperation();
    $data = $singleton->getSingletonData();
    echo $data;
}

main();
