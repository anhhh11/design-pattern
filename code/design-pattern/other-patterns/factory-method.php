<?php

namespace OtherPattern\FactoryMethod;

use PhpParser\Comment\Doc;
use function Sodium\add;

abstract class Document
{
    abstract function open();

    public function close()
    {
        echo "close document";
    }

    public function save()
    {
        echo "save document";
    }

    public function revert()
    {
        echo "revert document";
    }
}

class MyDocument extends Document
{

    function open()
    {
        return "some document";
    }
}

abstract class Application
{
    private $docs = array();

    abstract function createDocument(): Document;

    public function newDocument()
    {
        $doc = $this->createDocument();
        array_push($this->docs, $doc);
        $doc->open();
    }

    public function openDocument()
    {
        echo "Open document";
    }
}

class MyApplication extends Application
{
    function createDocument(): Document
    {
        return new MyDocument();
    }
}
