<?php

namespace OtherPattern\Bridge;

abstract class Executor
{
    abstract function query($queryObj);

    abstract function execute($command);
}


class MySQLExecutor extends Executor
{

    function query($queryObj)
    {
        // convert query obj to SQL and run
    }

    function execute($command)
    {
        // convert command to SQL and run
    }
}

class MongoDBExecutor extends Executor
{

    function query($queryObj)
    {
        // convert query obj to MongoDB query obj
    }

    function execute($command)
    {
        // convert query obj to MongoDB command obj
    }
}

class Repository
{
    public $executor;

    public function __construct($executor)
    {
        $this->executor = $executor;
    }

    function create($entity)
    {

    }

    function read()
    {

    }

    function update($entity)
    {

    }

    function delete($entity)
    {

    }

    function list($entity)
    {

    }
}

class UserRepo extends Repository
{

}

class UserService
{
    private $userRepo;

    public function __construct(UserRepo $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    function openAccount()
    {
        $this->userRepo->create(['id' => 123, 'password' => 123]);
        $this->userRepo->update(['balance' => 100]);
    }

    function closeAccount($userId)
    {
        $this->userRepo->update(['deleted' => true, 'id' => $userId]);
    }
}


function main()
{
    if (env('db') == 'mysql') {
        $executor = new MySQLExecutor();
    } else {
        $executor = new MongoDBExecutor();
    }
    $userRepo = new UserRepo($executor);
    $userService = new UserService($userRepo);
    $userService->openAccount();
    $userService->closeAccount(123);
}
