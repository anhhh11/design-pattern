<?php

namespace OtherPattern\Composite;

class Graphic
{
    function draw()
    {

    }

    function add($graphic)
    {

    }

    function remove($graphic)
    {

    }

    function getChild($int)
    {
    }
}

class Picture extends Graphic
{
    private $graphics = array();

    function draw()
    {
        foreach ($this->graphics as $graph) {
            $graph->draw();
        }
    }

    function add($graphic)
    {
        array_push($this->graphics, $graphic);
    }
}

class Line extends Graphic
{

    function draw()
    {
        // TODO: Implement draw() method.
    }
}

class Rectangle extends Graphic
{

    function draw()
    {
        // TODO: Implement draw() method.
    }
}

class Text extends Graphic
{

    function draw()
    {
        // TODO: Implement draw() method.
    }
}
