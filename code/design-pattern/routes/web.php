<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/abstract-factory/window', 'AbstractFactoryWindowClient@index');
Route::get('/abstract-factory/macos', 'AbstractFactoryMacOSClient@index');
Route::get('/adapter', 'AdapterClient@index');
Route::get('/chain-of-responsibility/{data}', 'ChainOfResponsibilityClient@index');
Route::get('/chain-of-responsibility/subclass/{data}', 'ChainOfResponsibilityClient@subclass');
