<?php

namespace App\Http\Controllers;

use App\Views\Widgets;
use Illuminate\Http\Request;

class AbstractFactoryWindowClient extends Controller
{
    protected $widgetFactory;

    public function __construct(Widgets\WidgetFactory $widgetFactory)
    {
        $this->widgetFactory = $widgetFactory;
    }

    function index()
    {
        return $this->widgetFactory->createWindow()->render() .
            $this->widgetFactory->createScrollbar()->render();
    }
}
