<?php

namespace App\Http\Controllers;


use App\Impl\Infrastructure\AuthMiddleware;
use App\Impl\Infrastructure\DefaultCombinedMiddleware;
use App\Impl\Infrastructure\ThrottleMiddleware;
use App\Infrastructure\Request;

class ChainOfResponsibilityClient extends Controller
{
    public function __construct()
    {
    }

    function index($data)
    {
        $auth = new AuthMiddleware();
        $throttle = new ThrottleMiddleware();
        $auth->setNext($throttle);
        $auth->handle(new Request($data));
    }

    function subclass($data)
    {
        $auth = new AuthMiddleware();
        $throttle = new ThrottleMiddleware();
        $defaultCombine = new DefaultCombinedMiddleware();
        $auth->setNext($throttle);
        $throttle->setNext($defaultCombine);
        $auth->handle(new Request($data));
    }
}
