<?php

namespace App\Http\Controllers;

use App\Entities\OrderInfo;
use App\Services\OrderService;

class AdapterClient extends Controller
{

    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    function index()
    {
        $this->orderService->sendOrderConfirmation(new OrderInfo());
        echo 'Done';
    }
}
