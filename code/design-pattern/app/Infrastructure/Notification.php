<?php

namespace App\Infrastructure;

interface Notification
{
    function notify($message);
}
