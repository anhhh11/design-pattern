<?php

namespace App\Infrastructure;

class Request
{
    private $data;

    function __construct(string $data)
    {
        $this->data = $data;
    }

    function getData()
    {
        return $this->data;
    }
}

abstract class Middleware
{
    /**
     * @var Middleware
     */
    private $next;

    function setNext(Middleware $next)
    {
        $this->next = $next;
    }

    function handle(Request $request)
    {
        if ($this->next != null) {
            $this->next->handle($request);
        }
    }
}
