<?php

namespace App\Views\MacOS\Widgets;

use App\Views\Widgets\Scrollbar;

class MacOSScrollbar extends Scrollbar
{
    function render(): string
    {
        return "MacOS scrollbar<br />";
    }
}
