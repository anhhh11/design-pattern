<?php

namespace App\Views\MacOS\Widgets;

use App\Views\Widgets\WidgetFactory;
use App\Views\Widgets\Scrollbar;
use App\Views\Widgets\Window;

class MacOSWidgetFactory extends WidgetFactory
{

    private static $instance = null;

    private function __construct()
    {
    }

    function createScrollbar(): Scrollbar
    {
        return new MacOSScrollbar();
    }

    function createWindow(): Window
    {
        return new MacOSWindow();
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new MacOSWidgetFactory();
        }

        return self::$instance;
    }
}
