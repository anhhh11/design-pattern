<?php

namespace App\Views\Widgets;
abstract class WidgetFactory
{
    abstract function createScrollbar(): Scrollbar;

    abstract function createWindow(): Window;

}
