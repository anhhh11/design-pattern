<?php

namespace App\Views\Widgets;
abstract class Window
{
    abstract function render();
}
