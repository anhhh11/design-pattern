<?php

namespace App\Views\Widgets;
abstract class Scrollbar
{
    abstract function render(): string;
}
