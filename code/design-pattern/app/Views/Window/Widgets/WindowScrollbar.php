<?php

namespace App\Views\Window\Widgets;

use App\Views\Widgets\Scrollbar;

class WindowScrollbar extends Scrollbar
{
    function render(): string
    {
        return "Window scrollbar<br />";
    }
}
