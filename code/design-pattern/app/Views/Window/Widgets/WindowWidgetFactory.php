<?php

namespace App\Views\Window\Widgets;

use App\Views\Widgets\WidgetFactory;
use App\Views\Widgets\Scrollbar;
use App\Views\Widgets\Window;

class WindowWidgetFactory extends WidgetFactory
{

    private static $instance = null;

    private function __construct()
    {
    }


    function createScrollbar(): Scrollbar
    {
        return new WindowScrollbar();
    }

    function createWindow(): Window
    {
        return new WindowWindow();
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new WindowWidgetFactory();
        }

        return self::$instance;
    }

}
