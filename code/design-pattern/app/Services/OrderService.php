<?php

namespace App\Services;


use App\Entities\OrderInfo;
use App\Impl\Infrastructure\EmailAdapter;
use App\Impl\Infrastructure\PHPMailer;

class OrderService
{
    private $notification;

    public function __construct()
    {
        $this->notification = new EmailAdapter(new PHPMailer());
    }

    function save($order, $orderRepo, $userRepo)
    {
    }

    function sendOrderConfirmation(OrderInfo $orderInfo)
    {
        $this->notification->notify($orderInfo->toString());
    }
}
