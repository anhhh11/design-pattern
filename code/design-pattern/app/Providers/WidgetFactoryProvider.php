<?php

namespace App\Providers;

use App\Http\Controllers\AbstractFactoryMacOSClient;
use App\Http\Controllers\AbstractFactoryWindowClient;
use App\Views\MacOS\Widgets\MacOSWidgetFactory;
use App\Views\Widgets\WidgetFactory;
use App\Views\Window\Widgets\WindowWidgetFactory;
use Illuminate\Support\ServiceProvider;

class WidgetFactoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(AbstractFactoryWindowClient::class)
            ->needs(WidgetFactory::class)
            ->give(function () {
                return WindowWidgetFactory::getInstance();
            });
        $this->app->when(AbstractFactoryMacOSClient::class)
            ->needs(WidgetFactory::class)
            ->give(function () {
                return MacOSWidgetFactory::getInstance();
            });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
