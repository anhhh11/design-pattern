<?php

namespace App\Impl\Infrastructure;

use App\Infrastructure\Notification;

class EmailAdapter implements Notification
{
    private $mailer;
    public function __construct(PHPMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    function notify($message)
    {
        echo "Notify message: " . $message;
    }
}
