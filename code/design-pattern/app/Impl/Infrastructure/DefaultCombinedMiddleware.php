<?php

namespace App\Impl\Infrastructure;

use App\Infrastructure\Middleware;
use App\Infrastructure\Request;

class DefaultCombinedMiddleware extends Middleware
{
    function handle(Request $request)
    {
        switch ($request->getData()) {
            case "ok":
                echo("Default combine handle OK");
                break;
            case "ng":
                echo("Default combine handle NG");
                break;
            default:
                parent::handle($request);
        }
    }
}
