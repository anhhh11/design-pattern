<?php

namespace App\Impl\Infrastructure;

use App\Infrastructure\Middleware;
use App\Infrastructure\Request;

class AuthMiddleware extends Middleware
{
    function handle(Request $request)
    {
        print("Auth handle request<br />");
        if ($request->getData() === "ok") {
            parent::handle($request);
        } else {
            print("Auth failed<br />");
        }
    }
}
