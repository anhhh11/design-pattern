<?php

namespace App\Impl\Infrastructure;

use App\Infrastructure\Middleware;
use App\Infrastructure\Request;

class ThrottleMiddleware extends Middleware
{
    function handle(Request $request)
    {
        print("Throttle handle request<br />");
        parent::handle($request);
    }
}
